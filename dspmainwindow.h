#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>
#include "ui_dspmainwindow.h"

namespace Ui {
class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DSPMainWindow(QWidget *parent = 0);
    ~DSPMainWindow();

private:
	Ui_MainWindow *ui;
	void plotWave(QVector<double>& xAxis, QVector<double>& yAxis, int & graphIndex);
	void plotFrequencyDomain(QVector<double>& xAxis, QVector<double>& yAxis, int & graphIndex);
	void initSlots();
	void initLineEditValidator();

private slots:
	void mf_PlotButton();
};

#endif // DSPMAINWINDOW_H
