#define _USE_MATH_DEFINES

#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

double sineWave(double f, double t, double fi)
{
	return 1 * sin(2 * M_PI * f * t + fi);
}

double squareWave(double x, double x0, double T)
{
	return 1 * pow(-1, int(2 * (x - x0) / T));
}

double sawtoothWave(double x, double T, double o)
{
	double var = ((x / T) + o);

	return 1 * (var - int(var));
}

double sweepWave(double o0, double f0, double t, double k)
{
	return sin(o0 + 2 * M_PI * (f0 * t + k / 2 * t * t));
}

void createSineWave(QVector<double>& xAxis, QVector<double>& yAxis, double frequency)
{
	for (int index = 0; index < 44100; index++)
	{
		xAxis[index] = index / 44100.0;
	}

	double T = 1.0 / 5000;
	for (int index = 0; index < 44100; index++)
	{
		yAxis[index] = sineWave(frequency, xAxis[index], 0);
	}
}

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui_MainWindow)
{
    ui->setupUi(this);

	ui->wavePlot->xAxis->setRange(0, 1);
	ui->wavePlot->yAxis->setRange(-1, 1);
	ui->frequencyPlot->xAxis->setRange(20, 2000);
	ui->frequencyPlot->yAxis->setRange(0, 400);

	//Only numbers for text boxes
	initLineEditValidator();

	//Initialize slots
	initSlots();
}

void DSPMainWindow::plotWave(QVector<double>& xAxis, QVector<double>& yAxis, int& graphIndex)
{
	ui->wavePlot->addGraph();
	ui->wavePlot->graph(graphIndex)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->wavePlot->graph(graphIndex)->setData(xAxis, yAxis);
	ui->wavePlot->replot();
}

void DSPMainWindow::plotFrequencyDomain(QVector<double>& xAxis, QVector<double>& yAxis, int& graphIndex)
{
	int FFTSize = 128;
	double frequencyRes = 44100.0 / FFTSize;
	auto fft = Aquila::FftFactory::getFft(FFTSize);
	auto spectrum = fft->fft(yAxis.toStdVector().data());

	QVector<double> amplitude, frequency;

	int index = 0;
	for each(auto item in spectrum)
	{
		amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
		frequency.push_back(index++ * frequencyRes);
	}

	ui->frequencyPlot->addGraph();
	ui->frequencyPlot->graph(graphIndex)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->frequencyPlot->graph(graphIndex)->setData(frequency, amplitude);
	ui->frequencyPlot->replot();

	graphIndex++;
}

void DSPMainWindow::mf_PlotButton()
{
	int graphIndex = 0;

	//Declare axis for wave
	QVector<double> xAxis(44100);
	QVector<double> yAxis(44100);

	//Create wave and plot wave #1
	createSineWave(xAxis, yAxis, ui->frequency1LineEdit->text().toDouble());
	plotWave(xAxis, yAxis, graphIndex);
	plotFrequencyDomain(xAxis, yAxis, graphIndex);

	//Create wave and plot wave #2
	createSineWave(xAxis, yAxis, ui->frequency2LineEdit->text().toDouble());
	plotWave(xAxis, yAxis, graphIndex);
	plotFrequencyDomain(xAxis, yAxis, graphIndex);

	//Create wave and plot wave #3
	createSineWave(xAxis, yAxis, ui->frequency3LineEdit->text().toDouble());
	plotWave(xAxis, yAxis, graphIndex);
	plotFrequencyDomain(xAxis, yAxis, graphIndex);

	//Create wave and plot wave #4
	createSineWave(xAxis, yAxis, ui->frequency4LineEdit->text().toDouble());
	plotWave(xAxis, yAxis, graphIndex);
	plotFrequencyDomain(xAxis, yAxis, graphIndex);

	//Create wave and plot wave #5
	createSineWave(xAxis, yAxis, ui->frequnecy5LineEdit->text().toDouble());
	plotWave(xAxis, yAxis, graphIndex);
	plotFrequencyDomain(xAxis, yAxis, graphIndex);
}

void DSPMainWindow::initSlots()
{
	QObject::connect(ui->plotPushButton, SIGNAL(released()), this, SLOT(mf_PlotButton()));
}

void DSPMainWindow::initLineEditValidator()
{
	ui->frequency1LineEdit->setValidator(new QIntValidator(0, 100, this));
	ui->frequency2LineEdit->setValidator(new QIntValidator(0, 100, this));
	ui->frequency3LineEdit->setValidator(new QIntValidator(0, 100, this));
	ui->frequency4LineEdit->setValidator(new QIntValidator(0, 100, this));
	ui->frequnecy5LineEdit->setValidator(new QIntValidator(0, 100, this));
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}
