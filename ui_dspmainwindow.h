/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_3;
    QLabel *frequency1Label;
    QLineEdit *frequency1LineEdit;
    QLineEdit *frequency4LineEdit;
    QLabel *frequency3Label;
    QLineEdit *frequency2LineEdit;
    QLabel *frequency2Label;
    QLabel *frequency5Label;
    QLineEdit *frequency3LineEdit;
    QLabel *frequency4Label;
    QLineEdit *frequnecy5LineEdit;
    QSpacerItem *horizontalSpacer;
    QPushButton *plotPushButton;
    QCustomPlot *frequencyPlot;
    QCustomPlot *wavePlot;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(700, 700);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        frequency1Label = new QLabel(centralWidget);
        frequency1Label->setObjectName(QStringLiteral("frequency1Label"));

        gridLayout_3->addWidget(frequency1Label, 0, 0, 1, 1);

        frequency1LineEdit = new QLineEdit(centralWidget);
        frequency1LineEdit->setObjectName(QStringLiteral("frequency1LineEdit"));

        gridLayout_3->addWidget(frequency1LineEdit, 0, 1, 1, 1);

        frequency4LineEdit = new QLineEdit(centralWidget);
        frequency4LineEdit->setObjectName(QStringLiteral("frequency4LineEdit"));

        gridLayout_3->addWidget(frequency4LineEdit, 0, 7, 1, 1);

        frequency3Label = new QLabel(centralWidget);
        frequency3Label->setObjectName(QStringLiteral("frequency3Label"));

        gridLayout_3->addWidget(frequency3Label, 0, 4, 1, 1);

        frequency2LineEdit = new QLineEdit(centralWidget);
        frequency2LineEdit->setObjectName(QStringLiteral("frequency2LineEdit"));

        gridLayout_3->addWidget(frequency2LineEdit, 0, 3, 1, 1);

        frequency2Label = new QLabel(centralWidget);
        frequency2Label->setObjectName(QStringLiteral("frequency2Label"));

        gridLayout_3->addWidget(frequency2Label, 0, 2, 1, 1);

        frequency5Label = new QLabel(centralWidget);
        frequency5Label->setObjectName(QStringLiteral("frequency5Label"));

        gridLayout_3->addWidget(frequency5Label, 0, 8, 1, 1);

        frequency3LineEdit = new QLineEdit(centralWidget);
        frequency3LineEdit->setObjectName(QStringLiteral("frequency3LineEdit"));

        gridLayout_3->addWidget(frequency3LineEdit, 0, 5, 1, 1);

        frequency4Label = new QLabel(centralWidget);
        frequency4Label->setObjectName(QStringLiteral("frequency4Label"));

        gridLayout_3->addWidget(frequency4Label, 0, 6, 1, 1);

        frequnecy5LineEdit = new QLineEdit(centralWidget);
        frequnecy5LineEdit->setObjectName(QStringLiteral("frequnecy5LineEdit"));
        frequnecy5LineEdit->setEnabled(true);

        gridLayout_3->addWidget(frequnecy5LineEdit, 0, 9, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 0, 11, 1, 1);

        plotPushButton = new QPushButton(centralWidget);
        plotPushButton->setObjectName(QStringLiteral("plotPushButton"));

        gridLayout_3->addWidget(plotPushButton, 0, 10, 1, 1);


        gridLayout->addLayout(gridLayout_3, 3, 1, 1, 1);

        frequencyPlot = new QCustomPlot(centralWidget);
        frequencyPlot->setObjectName(QStringLiteral("frequencyPlot"));

        gridLayout->addWidget(frequencyPlot, 1, 0, 1, 2);

        wavePlot = new QCustomPlot(centralWidget);
        wavePlot->setObjectName(QStringLiteral("wavePlot"));

        gridLayout->addWidget(wavePlot, 0, 0, 1, 2);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 700, 25));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "DisplayWindow", Q_NULLPTR));
        frequency1Label->setText(QApplication::translate("MainWindow", "Frequency #1", Q_NULLPTR));
        frequency3Label->setText(QApplication::translate("MainWindow", "Frequency #3", Q_NULLPTR));
        frequency2Label->setText(QApplication::translate("MainWindow", "Frequency #2", Q_NULLPTR));
        frequency5Label->setText(QApplication::translate("MainWindow", "Frequency #5", Q_NULLPTR));
        frequency4Label->setText(QApplication::translate("MainWindow", "Frequency #4", Q_NULLPTR));
        plotPushButton->setText(QApplication::translate("MainWindow", "Plot", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
